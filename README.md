# GALERIA DE FOTOS COM  Rails 3.2.3 + Carrierwave + jQuery 

Esta aplicação mostra como fazer upload de varios arquivos em tempo real.

Mais informações [jQuery File Upload](http://blueimp.github.com/jQuery-File-Upload/) ou em [Github](https://github.com/blueimp/jQuery-File-Upload).

Nós usamos [slimbox2](http://www.digitalia.be/software/slimbox2)  plugin para visualizar imagens.
E usamos plugin[jCrop](http://deepliquid.com/content/Jcrop.html) para edição de imagens.

# # Introdução

* Clone git:

          git clone git@bitbucket.org:hantys/galeria-fotos.git

* Mudar a pasta:

          cd galeria-fotos

* instalar as gems:

          bundle install

* criar database:

          rake db:migrate

* Atualize arquivos de inicialização:

          rails g bootstrap:install -f

* Comece servidor:

          rails s

* Abra o navegador:


          http://localhost:3000/galleries
# # Recursos

- (11-abr-2013): pode adicionar imagens a uma galeria quando é "novo" (ou seja, antes de ter sido salva)

